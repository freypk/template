Kubernetes Cluster
==================
--------------------------

Die Pipeline, legt automatisch 2 -3 VMs mit [microk8s](https://microk8s.io/) Kubernetes in der AWS, Azure oder in der Privaten Cloud (MAAS.io) an.

Die eigentlichen Pipelines befinden sich in den Repositories [aws](https://gitlab.com/mc-b-gitops/infra/aws), [azure](https://gitlab.com/mc-b-gitops/infra/azure) und [maas](https://gitlab.com/mc-b-gitops/infra/maas).

AWS Cloud
---------

Um die Pipeline zu benützen, sind vorgängig folgende Umgebungsvariablen, in der Untergruppe [infra](https://gitlab.com/groups/mc-b-gitops/infra/-/settings/ci_cd), zu setzen:

* AWS_CONFIG_FILE als "File" statt Variable ablegen. 

Der Inhalt kann z.B. bei AWS Academy, aus `AWS Details` -> `show`, übernommen werden.

    [default]
    aws_access_key_id=
    aws_secret_access_key=
    aws_session_token=

Die Ausgabe der Pipeline, bzw. dem Job `deploy` gibt die weiteren Befehle aus, für den Zugriff und die Verbindung der Worker-Nodes mit dem Master.   

Azure Cloud
-----------

Um die Pipeline zu benützen, sind vorgängig folgende Umgebungsvariablen, in der Untergruppe [infra](https://gitlab.com/groups/mc-b-gitops/infra/-/settings/ci_cd), zu setzen:

* AZURE_USER_ID - Username
* AZURE_USER_PW - Password

Die Authentifizierung mit der Azure Cloud findet mittels `az login` statt. 

Ist nicht die sicherste Methode, aber die einzige welche mit einem Azure Student Account funktioniert.

MAAS Cloud
----------

Das es leider keine Methode zu geben scheint die Gitlab Runner mit `--cap-add=NET_ADMIN` zu starten, muss der Zugriff auf das MAAS API im Internet sichtbar sein.

Würde `--cap-add=NET_ADMIN` funktionieren, könnte WireGuard inkl. Netzwerkadapter installiert werden.

So muss zuerst auf dem MAAS Server ein SSH Tunnel eingerichtet werden

    ssh -i config/ssh/ssh_tunnel -N -R 15240:localhost:5240 gateway

Und anschliessend folgende Umgebungsvariablen gesetzt werden:

* MAAS_URL - http://gateway:15240/MAAS
* MAAS_KEY - API Key
* MAAS_VPN - VPN welches für die VM verwendet werden soll.

Applikationen Deployen
----------------------

Die eigentlichen Applikationen befinden sich in der Untergruppe [apps](https://gitlab.com/mc-b-gitops/apps).

Diese werden, mittels Pipelines, auf obigen Kubernetes Cluster deployt.

Dafür ist vorgängig die Umgebungsvariable:

* KUBERNETES_CLUSTER

zu setzen.


Links
-----

* [How teams use GitLab and Terraform for infrastructure as code: A demo](https://about.gitlab.com/topics/gitops/gitlab-enables-infrastructure-as-code/)
* [Sourcecode](https://gitlab.com/gitops-demo)
* [GitLab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) - Variable TF_ADDRESS setzen!
* [Infrastructure as Code with Terraform and GitLab](https://docs.gitlab.com/ee/user/infrastructure/iac/)
* [Terraform http Backend](https://www.terraform.io/language/settings/backends/http)


